package com.saleweb.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author phongnd
 * @version 1.0
 * @since 1/12/2021 Class HrisException kế thừa Exception phục vụ việc xử lý và tổng hợp lỗi trong
 *     sourceCode.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ApplicationException extends Exception {
  private String messageKey;
  private String message;
  private List<String> messages;
  private Throwable throwable;

  public ApplicationException(String msg) {
    this.messages = new ArrayList<>(Arrays.asList(msg));
  }

  public ApplicationException(String msg, Throwable throwable) {
    this.messages = new ArrayList<>(Arrays.asList(msg));
    this.throwable = throwable;
  }

  public ApplicationException(List msgs) {
    this.messages = msgs;
  }

  public ApplicationException(String msgKey, String msg) {
    this.messageKey = msgKey;
    this.message = msg;
  }

  public String getMessage() {
    if (this.message != null) {
      return message;
    }
    if (this.messageKey != null) {
      this.message = String.format(ExceptionUtils.messages.get(this.messageKey));
    }
    return null;
  }

}
