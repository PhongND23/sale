package com.saleweb.exceptions;

import java.util.HashMap;
import java.util.Map;

public class ExceptionUtils {

  public static final String E_INTERNAL_SERVER = "E_INTERNAL_SERVER";
  public static final String E_CATEGORY_CODE_NULL = "E_CATEGORY_CODE_NULL";
  public static final String E_CATEGORY_NAME_NULL = "E_CATEGORY_NAME_NULL";
  public static final String E_CATEGORY_NAME_EXIST = "E_CATEGORY_NAME_EXIST";
  public static final String E_CATEGORY_CODE_EXIST = "E_CATEGORY_CODE_EXIST";
  public static final String E_CATEGORY_ID_NOT_EXIST = "E_CATEGORY_ID_NOT_EXIST";
  public static final String E_ROLE_ID_NOT_EXIST = "E_ROLE_ID_NOT_EXIST";
  public static final String E_ACCOUNT_ID_NOT_EXIST = "E_ACCOUNT_ID_NOT_EXIST";
  public static final String E_USER_ID_NOT_EXIST = "E_USER_ID_NOT_EXIST";
  public static final String E_USER_NAME_NULL = "E_USER_NAME_NULL";
  public static final String E_PASSWORD_NULL = "E_PASSWORD_NULL";
  public static final String E_POST_NAME_NULL = "E_POST_NAME_NULL";
  public static final String E_TYPE_NULL = "E_TYPE_NULL";
  public static final String E_ADDRESS_NULL = "E_ADDRESS_NULL";
  public static final String E_POST_ID_NOT_EXISTS = "E_POST_ID_NOT_EXISTS";
  public static final String E_EMAIL_NULL = "E_EMAIL_NULL";
  public static final String E_PHONE_NULL = "E_PHONE_NULL";
  public static final String E_PAYMENT_NULL = "E_PAYMENT_NULL";
  public static final String NULL = "NULL";
  public static final String NOT_EXIST = "NOT_EXIST";
  public static final String DUPLICATE_USERNAME = "DUPLICATE_USERNAME";
  public static final String ADDED_TO_CART = "ADDED_TO_CART";
  public static final String ACCOUNT_NOT_EXIST = "ACCOUNT_NOT_EXIST";
  public static final String ACCOUNT_NOT_VALID = "ACCOUNT_NOT_VALID";
  public static final String PASSWORD_NOT_VALID = "PASSWORD_NOT_VALID";
  public static final String USERNAME_NOT_VALID = "USERNAME_NOT_VALID";
  public static final String AUTHORIZE = "AUTHORIZE";
  public static final String LOCK_ACCOUNT = "LOCK_ACCOUNT";

  public static Map<String, String> messages;

  static {
    messages = new HashMap<>();
    messages.put(E_INTERNAL_SERVER, "Lỗi hệ thống");
    messages.put(E_CATEGORY_CODE_NULL, "Mã danh mục không được để trống !");
    messages.put(E_CATEGORY_NAME_NULL, "Tên danh mục không được để trống !");
    messages.put(E_CATEGORY_CODE_EXIST, "Mã danh mục đã tồn tại !");
    messages.put(E_CATEGORY_NAME_EXIST, "Tên danh mục đã tồn tại !");
    messages.put(E_CATEGORY_ID_NOT_EXIST, "Không tồn tại bản ghi có ID: %s");
    messages.put(E_ROLE_ID_NOT_EXIST, "Không tồn tại bản ghi có ID: %s");
    messages.put(E_ACCOUNT_ID_NOT_EXIST, "Không tồn tại bản ghi có ID: %s");
    messages.put(E_USER_ID_NOT_EXIST, "Không tồn tại user có ID: %s");
    messages.put(E_USER_NAME_NULL, "Tên đăng nhập không được để trống");
    messages.put(E_PASSWORD_NULL, "Mật khẩu không được để trống");
    messages.put(E_POST_NAME_NULL, "Tên không được để trống");
    messages.put(E_TYPE_NULL, "Loại được để trống");
    messages.put(E_POST_ID_NOT_EXISTS, "Không tồn tại bản ghi có ID: %s");
    messages.put(E_ADDRESS_NULL, "Địa chỉ không được để trống");
    messages.put(E_EMAIL_NULL, "Email không được để trống");
    messages.put(E_PHONE_NULL, "SĐT không được để trống");
    messages.put(E_PAYMENT_NULL, "Phương thức thanh toán không được để trống");
    messages.put(NULL, "% không được để trống");
    messages.put(NOT_EXIST, "% không tồn tại");
    messages.put(DUPLICATE_USERNAME, "Username đã tồn tại !");
    messages.put(ADDED_TO_CART, "Sản phẩm đã có trong giỏ hàng");
    messages.put(ACCOUNT_NOT_EXIST, "Tên tài khoản hoặc mật khẩu không đúng !");
    messages.put(PASSWORD_NOT_VALID, "Mật khẩu không đúng !");
    messages.put(PASSWORD_NOT_VALID, "Tên đăng nhập không đúng !");
    messages.put(AUTHORIZE, "Tài khoản không có quyền truy cập !");
    messages.put(LOCK_ACCOUNT, "Tài khoản đã bị khóa !");
    messages.put(ACCOUNT_NOT_VALID, "Tài khoản không hợp lệ !");
  }
}
