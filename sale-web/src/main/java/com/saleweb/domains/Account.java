package com.saleweb.domains;

import com.saleweb.domains.common.Auditable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.saleweb.models.account.AccountCreateDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "account")
public class Account extends Auditable {
  @Id
  @SequenceGenerator(
      name = "account_sequence_id",
      sequenceName = "account_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "username")
  private String userName;

  @Column(name = "password")
  private String password;

  @Column(name = "role_id")
  private Long roleId;

  @Column(name = "user_id")
  private Long userId;

  @Column(name = "status")
  private Boolean status;

  public Account(AccountCreateDTO dto)
  {
    this.setUserName(dto.getUserName());
    this.setPassword(dto.getPassword());
    this.setRoleId(Long.valueOf(2));
    this.setStatus(Boolean.TRUE);
  }
}
