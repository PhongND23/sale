package com.saleweb.domains;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.saleweb.models.category.PostCategoryCreateDTO;
import com.saleweb.domains.common.Auditable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "post_category")
public class PostCategory extends Auditable {
  @Id
  @SequenceGenerator(
      name = "post_category_sequence_id",
      sequenceName = "post_category_sequence_id",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "post_category_sequence_id")
  @Column(name = "id")
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "description")
  private String description;

  @JsonProperty
  @Column(name = "status")
  private Boolean status;

  public PostCategory(PostCategoryCreateDTO dto) {
    this.name = dto.getName();
    this.description = dto.getDescription();
    this.status = true;
  }
}
