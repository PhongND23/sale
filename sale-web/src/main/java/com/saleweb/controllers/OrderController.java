package com.saleweb.controllers;

import com.saleweb.common.MessageDTO;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.cart.ProductCartDTO;
import com.saleweb.services.CartService;
import com.saleweb.services.OrderService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/order")
@RestController
public class OrderController {
  private final OrderService service;


  @CrossOrigin
  @PostMapping(value = "/payment/{accountId}/{paymentId}",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> payment(@PathVariable Long accountId,@PathVariable Long paymentId,@RequestBody List<ProductCartDTO> productCartDTOS) {
    try {
      service.payment(accountId,paymentId,productCartDTOS);
      return new ResponseEntity<>(HttpStatus.OK);
    }catch (ApplicationException ex)
    {
      return new ResponseEntity<>(
          new MessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getOrder(@PathVariable Long id) {
    try {
      return new ResponseEntity<>(service.getOrder(id),HttpStatus.OK);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getAll(@Parameter(hidden = true) Pageable pageable) {
    try {
      return new ResponseEntity<>(service.getAll(pageable),HttpStatus.OK);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "/status/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getAllByStatus(@Parameter(hidden = true) Pageable pageable,@PathVariable Long id) {
    try {
      return new ResponseEntity<>(service.getAllByStatus(pageable,id),HttpStatus.OK);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @CrossOrigin
  @GetMapping(value = "change/{id}/{statusId}",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> changeStatus(@PathVariable Long id,@PathVariable Long statusId) {
    try {
      service.changeStatus(id,statusId);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "buy/{accountId}/{productId}/{quantity}/{paymentId}",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> buyOneProduct(@PathVariable Long accountId,@PathVariable Long productId,@PathVariable int quantity,@PathVariable Long paymentId
  ) {
    try {
      service.buyNow(accountId,productId,quantity,paymentId);
      return new ResponseEntity<>(HttpStatus.OK);
    }catch (ApplicationException ex)
    {
      return new ResponseEntity<>(new MessageDTO(ex.getMessage()),HttpStatus.BAD_REQUEST);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "of-account/{accountId}",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> buyOneProduct(@PathVariable Long accountId) {
    try {

      return new ResponseEntity<>(service.getOrderByAccountId(accountId),HttpStatus.OK);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @GetMapping(value = "cancel/{orderId}/{accountId}",produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> cancel(@PathVariable Long orderId,@PathVariable Long accountId) {
    try {

      service.cancelOrder(orderId,accountId);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    catch (Exception ex) {
      log.error(ex.getMessage());
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
