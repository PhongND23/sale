package com.saleweb.models.cart;

import com.saleweb.domains.Cart;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CartDTO {

    private Long id;

    private Long account_id;

    private List<ProductCartDTO> productCartDTOs;

    public CartDTO(Cart cart, List<ProductCartDTO> productCartDTOs)
    {
        this.setId(cart.getId());
        this.setAccount_id(cart.getAccountId());
        this.setProductCartDTOs(productCartDTOs);
    }
}
