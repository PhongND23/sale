package com.saleweb.models.cart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.saleweb.domains.CartDetail;
import com.saleweb.domains.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ProductCartDTO {
  private Long id;

  private String name;

  private Integer quantity;

  private Double price;

  private Double promotionPrice;
  @JsonProperty private Boolean isBuy;

  private String firstImage;

  public ProductCartDTO(CartDetail product) {
    this.setId(product.getProductId());
    this.setQuantity(product.getQuantity());
    this.setPrice(product.getPrice());
    this.setPromotionPrice(product.getPromotionPrice());
    this.setIsBuy(Boolean.FALSE);
  }
  public ProductCartDTO(Product product) {
    this.setId(product.getId());
    this.setQuantity(product.getQuantity());
    this.setPrice(product.getPrice());
    this.setPromotionPrice(product.getPromotionPrice());
    this.setIsBuy(Boolean.TRUE);
  }
}
