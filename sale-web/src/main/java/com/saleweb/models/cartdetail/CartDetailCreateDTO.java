package com.saleweb.models.cartdetail;

import com.saleweb.models.cart.ProductCartDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CartDetailCreateDTO {

    private Long accountId;

    private Long payment_id;

}
