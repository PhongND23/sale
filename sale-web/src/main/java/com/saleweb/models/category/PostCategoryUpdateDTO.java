package com.saleweb.models.category;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PostCategoryUpdateDTO {
  private String name;
  private String description;
  @JsonProperty
  private Boolean status;

}
