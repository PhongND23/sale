package com.saleweb.models.account;

import com.saleweb.models.user.UserCreateDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class AccountCreateDTO {

    private String userName;

    private String password;

    private Long roleId;

    private UserCreateDTO userDTO;

}
