package com.saleweb.models.product;

import com.saleweb.domains.Product;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProductDTO {
  private Long id;

  private String name;

  private Long categoryId;

  private String categoryName;

  private List<String> images;

  private Double price;

  private Double promotionPrice;

  private Integer quantity;

  private String description;

  private String content;

  private Integer viewCount;
  private Integer likeCount;

  private Boolean status;

  private String classify;

  private String rate;

  private String trademark;
  private Boolean isHot;



  private String tags;

  public ProductDTO(Product product,String categoryName,List<String> images)
  {
    this.setId(product.getId());
    this.setName(product.getName());
    this.setCategoryId(product.getCategoryId());
    this.setCategoryName(categoryName);
    this.setImages(images);
    this.setPrice(product.getPrice());
    this.setPromotionPrice(product.getPromotionPrice());
    this.setQuantity(product.getQuantity());
    this.setDescription(product.getDescription());
    this.setContent(product.getContent());
    this.setViewCount(product.getViewCount());
    this.setLikeCount(product.getLikeCount());
    this.setStatus(product.getStatus());
    this.setClassify(product.getClassify());
    this.setRate(product.getRate());
    this.setTrademark(product.getTrademark());
    this.setTags(product.getTags());
    this.setIsHot(product.getIsHot());
  }
  public ProductDTO(Product product)
  {
    this.setId(product.getId());
    this.setName(product.getName());
    this.setCategoryId(product.getCategoryId());
    this.setPrice(product.getPrice());
    this.setPromotionPrice(product.getPromotionPrice());
    this.setQuantity(product.getQuantity());
    this.setDescription(product.getDescription());
    this.setContent(product.getContent());
    this.setViewCount(product.getViewCount());
    this.setLikeCount(product.getLikeCount());
    this.setStatus(product.getStatus());
    this.setClassify(product.getClassify());
    this.setRate(product.getRate());
    this.setTrademark(product.getTrademark());
    this.setTags(product.getTags());
    this.setIsHot(product.getIsHot());
  }

}
