package com.saleweb.models.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UserUpdateDTO {

    private String fullName;

    private String address;

    private String email;

    private String phoneNumber;

    @JsonProperty
    private Boolean gender;

    private LocalDate dateOfBirth;

}
