package com.saleweb.models.dashboard;

import com.saleweb.domains.OrderStatus;
import com.saleweb.domains.PostCategory;
import com.saleweb.domains.ProductCategory;
import com.saleweb.domains.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class DashboardDTO {
    private Integer countOrder;
    private Integer countAccount;
    private Integer countProductCategory;
    private Integer countPostCategory;
    private Integer countPost;
    private Integer countProduct;
}
