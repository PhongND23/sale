package com.saleweb.services;

import com.saleweb.models.category.DropDownDTO;

import java.util.List;

public interface OrderStatusService {
    List<DropDownDTO> getDrop();
}
