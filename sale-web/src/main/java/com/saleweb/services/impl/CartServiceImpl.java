package com.saleweb.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saleweb.domains.Cart;
import com.saleweb.domains.CartDetail;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.cart.CartDTO;
import com.saleweb.models.cart.ProductCartDTO;
import com.saleweb.repositories.AccountRepository;
import com.saleweb.repositories.CartDetailRepository;
import com.saleweb.repositories.CartRepository;
import com.saleweb.repositories.ProductRepository;
import com.saleweb.services.CartService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {
  private final CartRepository cartRepository;
  private final AccountRepository accountRepository;
  private final ProductRepository productRepository;
  private final CartDetailRepository cartDetailRepository;

  @Override
  public void addToCart(Long accountId, Long productId) throws ApplicationException {
    var accountOptional = accountRepository.findById(accountId);
    if (accountOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Tài khoản "));
    }

    var productOptional = productRepository.findById(productId);
    if (productOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Sản phẩm "));
    }
    var cartOptional = cartRepository.findByAccountId(accountId);
    if (cartOptional.isPresent()) {
      var cart = cartOptional.get();
      var cartDetailOptional = cartDetailRepository.findByCartIdAndProductId(cart.getId(), productId);
      if (cartDetailOptional.isPresent()) {
        cartDetailOptional.get().setQuantity(cartDetailOptional.get().getQuantity()+1);
        cartDetailRepository.save(cartDetailOptional.get());
        return;
      }
      CartDetail cartDetail = new CartDetail(cart.getId(), productId, productOptional.get());
      cartDetailRepository.save(cartDetail);
    } else {
      Cart cart = new Cart(accountId);
      cartRepository.save(cart);
      CartDetail cartDetail = new CartDetail(cart.getId(), productId, productOptional.get());
      cartDetailRepository.save(cartDetail);
    }
  }

  @Override
  public void addToCart(Long accountId, Long productId, int quantity) throws ApplicationException {
    var accountOptional = accountRepository.findById(accountId);
    if (accountOptional.isEmpty()) {
      throw new ApplicationException(
              ExceptionUtils.NOT_EXIST,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Tài khoản "));
    }

    var productOptional = productRepository.findById(productId);
    if (productOptional.isEmpty()) {
      throw new ApplicationException(
              ExceptionUtils.NOT_EXIST,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Sản phẩm "));
    }
    if(Objects.isNull(quantity))
    {
      throw new ApplicationException(
              ExceptionUtils.NOT_EXIST,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Số lượng "));
    }
    var cartOptional = cartRepository.findByAccountId(accountId);
    if (cartOptional.isPresent()) {
      var cart = cartOptional.get();
      var cartDetailOptional = cartDetailRepository.findByCartIdAndProductId(cart.getId(), productId);
      if (cartDetailOptional.isPresent()) {
        cartDetailOptional.get().setQuantity(cartDetailOptional.get().getQuantity()+quantity);
        cartDetailRepository.save(cartDetailOptional.get());
        return;
      }

      CartDetail cartDetail = new CartDetail(cart.getId(), productId, productOptional.get());
      cartDetail.setQuantity(quantity);
      cartDetailRepository.save(cartDetail);
    } else {
      Cart cart = new Cart(accountId);
      cartRepository.save(cart);
      CartDetail cartDetail = new CartDetail(cart.getId(), productId, productOptional.get());
      cartDetail.setQuantity(quantity);
      cartDetailRepository.save(cartDetail);
    }
  }

  @Override
  public void removeToCart(Long accountId, Long productId) throws ApplicationException {
    var accountOptional = accountRepository.findById(accountId);
    if (accountOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Tài khoản "));
    }

    var productOptional = productRepository.findById(productId);
    if (productOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Sản phẩm "));
    }
    var cartOptional = cartRepository.findByAccountId(accountId);
    if (cartOptional.isEmpty()) {
      throw new ApplicationException("ERROR", "Có lỗi khi xóa !");
    }
    var cart = cartOptional.get();
    var cartDetailOptional = cartDetailRepository.findByCartIdAndProductId(cart.getId(), productId);
    if (cartDetailOptional.isEmpty()) {
      throw new ApplicationException("ERROR", "Có lỗi khi xóa !");
    }
    cartDetailRepository.delete(cartDetailOptional.get());
  }

  @Override
  @Transactional
  public void updateCart(Long accountId, List<ProductCartDTO> list) throws ApplicationException {
    var accountOptional = accountRepository.findById(accountId);
    if (accountOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Tài khoản "));
    }
    var account=accountOptional.get();
    var cartOptional=cartRepository.findByAccountId(accountId);
    if (cartOptional.isEmpty()) {
      throw new ApplicationException(
              ExceptionUtils.NOT_EXIST,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Giỏ hàng "));
    }
    List<CartDetail> cartDetails=new ArrayList<>();
    var oldCartDetails=cartDetailRepository.findByCartId(cartOptional.get().getId());
    var productIds=list.stream().map(ProductCartDTO::getId).collect(Collectors.toList());
    var products=productRepository.findByIdIn(productIds);
    cartDetailRepository.deleteAll(oldCartDetails);
    list.forEach(
        i -> {
          var product = products.stream().filter(x -> i.getId().equals(i.getId())).findFirst();
          cartDetails.add(new CartDetail(cartOptional.get().getId(),i.getId(),product.get(),i.getQuantity()));

        });
    cartDetailRepository.saveAll(cartDetails);
  }

  @Override
  public CartDTO getCart(Long accountId) {
    try {

      var cart = cartRepository.findByAccountId(accountId).get();
      var productsInCart =
          cartDetailRepository.findByCartId(cart.getId()).stream()
              .map(ProductCartDTO::new)
              .collect(Collectors.toList());
      var productIds =
          productsInCart.stream().map(ProductCartDTO::getId).collect(Collectors.toList());
      ObjectMapper mapper = new ObjectMapper();

      var products = productRepository.findByIdIn(productIds);
      Map<Long, List<String>> imagesMap = new HashMap<>();
      Map<Long, String> namesMap = new HashMap<>();

      products.forEach(
          i -> {
            try {
              var images = Arrays.asList(mapper.readValue(i.getImages(), String[].class));
              imagesMap.put(i.getId(), images);
              namesMap.put(i.getId(), i.getName());
            } catch (JsonProcessingException ex) {

              imagesMap.put(i.getId(), List.of("no_image.png"));
            }
          });

      productsInCart.forEach(i -> {
        i.setFirstImage(imagesMap.get(i.getId()).get(0));
        i.setName(namesMap.get(i.getId()));
      });
      return new CartDTO(cart, productsInCart);
    } catch (Exception ex) {
      return new CartDTO();
    }
  }
}
