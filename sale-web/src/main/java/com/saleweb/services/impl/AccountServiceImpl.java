package com.saleweb.services.impl;

import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Account;
import com.saleweb.domains.User;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.account.*;
import com.saleweb.models.role.RoleDTO;
import com.saleweb.models.user.UserDTO;
import com.saleweb.repositories.AccountRepository;
import com.saleweb.repositories.RoleRepository;
import com.saleweb.repositories.UsersRepository;
import com.saleweb.services.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {

  private final AccountRepository repository;
  private final RoleRepository roleRepository;
  private final UsersRepository usersRepository;
  private final AccountRepository accountRepository;

  @Override
  public void create(AccountCreateDTO dto) throws ApplicationException {
    // check userName
    if (StringUtils.isBlank(dto.getUserName())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Username "));
    }
    if(repository.existsByUserName(dto.getUserName()))
    {
      throw new ApplicationException(
              ExceptionUtils.DUPLICATE_USERNAME,
             ExceptionUtils.messages.get(ExceptionUtils.DUPLICATE_USERNAME));
    }
    if (StringUtils.isBlank(dto.getPassword())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Password "));
    }
    if (Objects.isNull(dto.getRoleId())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Quyền "));
    }
    // check roleId
    if (!roleRepository.existsById(dto.getRoleId())) {
      throw new ApplicationException(
          ExceptionUtils.E_ROLE_ID_NOT_EXIST,
          String.format(
              ExceptionUtils.messages.get(ExceptionUtils.E_ROLE_ID_NOT_EXIST), dto.getRoleId()));
    }
    // check userID
    if (Objects.isNull(dto.getUserDTO())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Thông tin cá nhân "));
    }

    if (StringUtils.isBlank(dto.getUserDTO().getFullName())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Họ tên "));
    }
    if (StringUtils.isBlank(dto.getUserDTO().getAddress())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Địa chỉ "));
    }

    if (StringUtils.isBlank(dto.getUserDTO().getPhoneNumber())||Objects.isNull(dto.getUserDTO().getPhoneNumber())) {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Số điện thoại "));
    }
    User user = new User(dto.getUserDTO());
    usersRepository.save(user);
    Account account = new Account(dto);
    account.setUserId(user.getId());
    repository.save(account);
  }

  @Override
  public void update(AccountUpdateDTO dto, Long id) throws ApplicationException {
    Optional<Account> account = repository.findById(id);
    if (account.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_ACCOUNT_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_ACCOUNT_ID_NOT_EXIST), id));
    }
    if (StringUtils.isBlank(dto.getPassword())) {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Password "));
    }
    if (dto.getRoleId() == null) {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Quyền "));
    }
    if (!roleRepository.existsById(dto.getRoleId())) {
      throw new ApplicationException(
              ExceptionUtils.E_ROLE_ID_NOT_EXIST,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.E_ROLE_ID_NOT_EXIST), dto.getRoleId()));
    }
    account.get().setPassword(dto.getPassword());
    account.get().setRoleId(dto.getRoleId());
    account.get().setStatus(dto.getStatus());
    repository.save(account.get());
  }

  @Override
  public void delete(Long id) throws ApplicationException {
    if (!repository.existsById(id)) {
      throw new ApplicationException(
          ExceptionUtils.E_ACCOUNT_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_ACCOUNT_ID_NOT_EXIST), id));
    }
    repository.deleteById(id);
  }

  @Override
  public AccountDTO findById(Long id) throws ApplicationException {
    Optional<Account> byId = repository.findById(id);

    var userOptional=usersRepository.findById(byId.get().getUserId());
    var roleOptional=roleRepository.findById(byId.get().getRoleId());
    if(userOptional.isPresent()&&roleOptional.isPresent()&&byId.isPresent())
    {
      return new AccountDTO(byId.get(),new RoleDTO(roleOptional.get()),new UserDTO(userOptional.get()));
    }
    return new AccountDTO();
  }

  @Override
  public Page<AccountDTO> getAll(SearchDTO dto, Pageable pageable) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<Account> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (StringUtils.isNotBlank(dto.getUserName())) {
            predicates.add(criteriaBuilder.like(root.get("userName"),"%"+ dto.getUserName()+"%"));
          }

          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    Page<Account> all = repository.findAll(specification, pageable);
    List<AccountDTO> list=new ArrayList<>();
    var roleDTOs= IterableUtils.toList(roleRepository.findAll()).stream().map(RoleDTO::new).collect(Collectors.toList());
    Map<Long,RoleDTO> roleDTOMap=new HashMap<>();
    roleDTOs.forEach(i->roleDTOMap.put(i.getId(),i));

    var userDTOs= IterableUtils.toList(usersRepository.findAll()).stream().map(UserDTO::new).collect(Collectors.toList());
    Map<Long,UserDTO> userDTOMap=new HashMap<>();
    userDTOs.forEach(i->userDTOMap.put(i.getId(),i));
    all.forEach(i->list.add(new AccountDTO(i,roleDTOMap.get(i.getRoleId()),userDTOMap.get(i.getUserId()))));
    return new PageImpl<>(list,pageable,all.getTotalElements());
  }

  @Override
  public AccountQueryDTO Login(Login login) throws ApplicationException {
    var accountOptional=accountRepository.getByUsernameAndPassword(login.getUserName(),login.getPassword());
    if(accountOptional==null)
    {
      throw new ApplicationException(ExceptionUtils.ACCOUNT_NOT_EXIST,ExceptionUtils.messages.get(ExceptionUtils.ACCOUNT_NOT_EXIST));
    }
    if(accountOptional.getRoleId()==1)
    {
      throw new ApplicationException(ExceptionUtils.ACCOUNT_NOT_VALID,ExceptionUtils.messages.get(ExceptionUtils.ACCOUNT_NOT_VALID));
    }
    if(BooleanUtils.isFalse(accountOptional.getStatus()))
    {
      throw new ApplicationException(ExceptionUtils.LOCK_ACCOUNT,ExceptionUtils.messages.get(ExceptionUtils.LOCK_ACCOUNT));
    }
    return accountOptional;
  }

  @Override
  public AccountQueryDTO LoginAdmin(Login login) throws ApplicationException {
    var accountOptional=accountRepository.getByUsernameAndPassword(login.getUserName(),login.getPassword());
    if(accountOptional==null)
    {
      throw new ApplicationException(ExceptionUtils.ACCOUNT_NOT_EXIST,ExceptionUtils.messages.get(ExceptionUtils.ACCOUNT_NOT_EXIST));
    }
    if(accountOptional.getRoleId()!=1)
    {
      throw new ApplicationException(ExceptionUtils.AUTHORIZE,ExceptionUtils.messages.get(ExceptionUtils.AUTHORIZE));
    }
    if(BooleanUtils.isFalse(accountOptional.getStatus()))
    {
      throw new ApplicationException(ExceptionUtils.LOCK_ACCOUNT,ExceptionUtils.messages.get(ExceptionUtils.LOCK_ACCOUNT));
    }
    return accountOptional;
  }

  @Override
  public void changePass(Long id,String pass) {
    var account=accountRepository.findById(id);
    if(account.isPresent())
    {
      account.get().setPassword(pass);
      accountRepository.save(account.get());
    }
  }
}
