package com.saleweb.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Post;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.post.PostCreateDTO;
import com.saleweb.models.post.PostDTO;
import com.saleweb.models.post.PostUpdateDTO;
import com.saleweb.repositories.PostCategoryRepository;
import com.saleweb.repositories.PostRepository;
import com.saleweb.services.PostService;
import io.swagger.v3.core.util.Json;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class PostServiceImpl implements PostService {

  private final PostRepository repository;
  private final PostCategoryRepository postCategoryRepository;
  private final ObjectMapper mapper;

  @Override
  public void create(PostCreateDTO dto) throws ApplicationException {
    // check name
    if (Objects.isNull(dto.getCategoryId())) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_NAME_NULL,
          ExceptionUtils.messages.get(ExceptionUtils.E_POST_NAME_NULL));
    }
    // check loại
    var categoryOptional = postCategoryRepository.findById(dto.getCategoryId());
    if (categoryOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(
              ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST),
              dto.getCategoryId()));
    }
    if (dto.getImages().isEmpty() || Objects.isNull(dto.getImages())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Ảnh "));
    }
    if(StringUtils.isBlank(dto.getSortDescription()))
    {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Mô tả ngắn "));
    }
    if(StringUtils.isBlank(dto.getContent()))
    {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Nội dung "));
    }
    if(StringUtils.isBlank(dto.getTitle()))
    {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tiêu đề "));
    }
    if(StringUtils.isBlank(dto.getTags()))
    {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tag "));
    }
    try{
      var images=mapper.writeValueAsString(dto.getImages());
      repository.save(new Post(dto,images));
    }catch (JsonProcessingException ex)
    {
      throw new ApplicationException(ExceptionUtils.E_INTERNAL_SERVER,ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER));
    }
  }

  @Override
  public void update(PostUpdateDTO dto, Long id) throws ApplicationException {
    Optional<Post> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    if (Objects.isNull(dto.getCategoryId())) {
      throw new ApplicationException(
              ExceptionUtils.E_POST_NAME_NULL,
              ExceptionUtils.messages.get(ExceptionUtils.E_POST_NAME_NULL));
    }
    // check loại
    var categoryOptional = postCategoryRepository.findById(dto.getCategoryId());
    if (categoryOptional.isEmpty()) {
      throw new ApplicationException(
              ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
              String.format(
                      ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST),
                      dto.getCategoryId()));
    }
    if (dto.getImages().isEmpty() || Objects.isNull(dto.getImages())) {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Ảnh "));
    }
    if(StringUtils.isBlank(dto.getContent()))
    {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Nội dung "));
    }
    if(StringUtils.isBlank(dto.getSortDescription()))
    {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Mô tả ngắn "));
    }
    if(StringUtils.isBlank(dto.getTitle()))
    {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tiêu đề "));
    }
    if(StringUtils.isBlank(dto.getTags()))
    {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tag "));
    }
    try{
      var images=mapper.writeValueAsString(dto.getImages());
      change(byId.get(),dto,images);
      repository.save(byId.get());
    }catch (JsonProcessingException ex)
    {
      throw new ApplicationException(ExceptionUtils.E_INTERNAL_SERVER,ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER));
    }
  }

  private void change(Post post, PostUpdateDTO dto,String images) {
    post.setTitle(dto.getTitle());
    post.setCategoryId(dto.getCategoryId());
    post.setContent(dto.getContent());
    post.setTags(dto.getTags());
    post.setImages(images);
    post.setIsHot(dto.getIsHot());
    post.setStatus(dto.getStatus());
    post.setSortDescription(dto.getSortDescription());
  }

  @Override
  public void delete(Long id) throws ApplicationException {
    Optional<Post> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    repository.delete(byId.get());
  }

  @Override
  public PostDTO findById(Long id) throws ApplicationException, JsonProcessingException {
    Optional<Post> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
   try{
     var images=mapper.readValue(byId.get().getImages(),List.class);
     var category=postCategoryRepository.findById(byId.get().getCategoryId());
     return new PostDTO(byId.get(),images,category.get().getName());
   }catch (JsonProcessingException ex)
   {
     return new PostDTO();
   }
  }

  @Override
  public Page<PostDTO> getAll(SearchDTO dto, Pageable pageable) throws JsonProcessingException {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<Post> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (StringUtils.isNotBlank(dto.getName())) {
            predicates.add(criteriaBuilder.like(root.get("title"), "%"+dto.getName()+"%"));
          }
          if (!Objects.isNull(dto.getCategoryId())) {
            predicates.add(criteriaBuilder.equal(root.get("categoryId"), dto.getCategoryId()));
          }
          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    Page<Post> all = repository.findAll(specification, pageable);
    Map<Long,String> maps=new HashMap<>();
    var categories= IterableUtils.toList(postCategoryRepository.findAll());
    categories.forEach(i->maps.put(i.getId(),i.getName()));
    List<PostDTO> postCreateDTOS = new ArrayList<>();
    for (Post p : all.getContent()) {
      try{
        var images=mapper.readValue(p.getImages(), List.class);
        postCreateDTOS.add(new PostDTO(p,images,maps.get(p.getCategoryId())));
      }
      catch (JsonProcessingException ex)
      {
        postCreateDTOS.add(new PostDTO(p,new ArrayList<>(),StringUtils.EMPTY));
      }
    }
    return new PageImpl<>(postCreateDTOS, pageable, all.getTotalElements());
  }
}
