package com.saleweb.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saleweb.domains.Order;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.cart.ProductCartDTO;
import com.saleweb.models.category.DropDownDTO;
import com.saleweb.models.order.OrderDTO;
import com.saleweb.repositories.*;
import com.saleweb.services.CartService;
import com.saleweb.services.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

  private final OrderRepository orderRepository;
  private final CartService cartService;
  private final CartRepository cartRepository;
  private final CartDetailRepository cartDetailRepository;
  private final PaymentsRepository paymentsRepository;
  private final OrdersStatusRepository ordersStatusRepository;
  private final AccountRepository accountRepository;
  private final ProductRepository productRepository;

  @Override

  public void payment(Long accountId, Long paymentId, List<ProductCartDTO> list)
      throws ApplicationException {

    try {
      ObjectMapper mapper = new ObjectMapper();
      var products =
          list.stream().filter(i -> BooleanUtils.isTrue(i.getIsBuy())).collect(Collectors.toList());
      var productsJson = mapper.writeValueAsString(products);
      Order order = new Order(accountId, paymentId, productsJson);
      orderRepository.save(order);
      var productIds = products.stream().map(ProductCartDTO::getId).collect(Collectors.toList());
      var cartOfAccount = cartRepository.findByAccountId(accountId);
      var productCarts = IterableUtils.toList(cartDetailRepository.findAll());
      var cartDetails =
          productCarts.stream()
              .filter(i -> productIds.contains(i.getProductId())&&i.getCartId().equals(cartOfAccount.get().getId()))
              .collect(Collectors.toList());
      cartDetailRepository.deleteAll(cartDetails);
    } catch (JsonProcessingException ex) {

    }
  }

  @Override
  public OrderDTO getOrder(Long id) {

    var order= orderRepository.getOderById(id);
    ObjectMapper mapper=new ObjectMapper();
    try{
      var products=Arrays.asList(mapper.readValue(order.getProductsString(),ProductCartDTO[].class));
      double total=0d;
      for(var item : products){
        if(item.getPromotionPrice()!=0)
        {
          total+=item.getPromotionPrice()*item.getQuantity();
        }
        else
        {
          total+=item.getPrice()*item.getQuantity();
        }
      }
      order.setProducts(products);
      order.setTotal(total);
    }catch (JsonProcessingException ex)
    {
      order.setProducts(Collections.emptyList());
    }
    return order;
  }

  @Override
  public Page<OrderDTO> getAll(Pageable pageable) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    var orders=orderRepository.getOrders(pageable);
    ObjectMapper mapper=new ObjectMapper();
    orders.getContent().forEach(i->{
      try{
        var products=Arrays.asList(mapper.readValue(i.getProductsString(),ProductCartDTO[].class));
        double total=0d;
        for(var item : products){
          if(item.getPromotionPrice()!=0)
          {
            total+=item.getPromotionPrice()*item.getQuantity();
          }
          else
          {
            total+=item.getPrice()*item.getQuantity();
          }
        }
        i.setProducts(products);
        i.setTotal(total);
      }catch (JsonProcessingException ex)
      {
        i.setProducts(Collections.emptyList());
      }
    });
    return new PageImpl<>(orders.getContent(),pageable,orders.getTotalElements());
  }

  @Override
  public Page<OrderDTO> getAllByStatus(Pageable pageable, Long id) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    var orders=orderRepository.getOrdersByStatus(pageable,id);
    ObjectMapper mapper=new ObjectMapper();
    orders.getContent().forEach(i->{
      try{
        var products=Arrays.asList(mapper.readValue(i.getProductsString(),ProductCartDTO[].class));
        double total=0d;
        for(var item : products){
          if(item.getPromotionPrice()!=0)
          {
            total+=item.getPromotionPrice()*item.getQuantity();
          }
          else
          {
            total+=item.getPrice()*item.getQuantity();
          }
        }
        i.setProducts(products);
        i.setTotal(total);
      }catch (JsonProcessingException ex)
      {
        i.setProducts(Collections.emptyList());
      }
    });
    return new PageImpl<>(orders.getContent(),pageable,orders.getTotalElements());
  }

  @Override
  public void changeStatus(Long id, Long statusId) {
    var orderOptional=orderRepository.findById(id);
    if(orderOptional.isPresent())
    {
      orderOptional.get().setOrderStatusId(statusId);
      orderRepository.save(orderOptional.get());
    }
  }

  @Override
  public void buyNow(Long accountId,Long productId,Integer quantity,Long paymentId) throws ApplicationException {
    var accountOptional=accountRepository.findById(accountId);
    if(accountOptional.isPresent())
    {
      var account=accountOptional.get();
      var productOptional=productRepository.findById(productId);
      if(productOptional.isPresent())
      {
        ProductCartDTO productCartDTO=new ProductCartDTO(productOptional.get());
        productCartDTO.setQuantity(quantity);
        try{
          ObjectMapper mapper=new ObjectMapper();
          var firstImage=mapper.readValue(productOptional.get().getImages(),String[].class);
          productCartDTO.setFirstImage(firstImage[0]);
          List<ProductCartDTO> productCartDTOS=new ArrayList<>();
          productCartDTOS.add(productCartDTO);
          var productsJson=mapper.writeValueAsString(productCartDTOS);
          orderRepository.save(new Order(accountId,paymentId,productsJson));

        }catch (JsonProcessingException ex)
        {
          throw new ApplicationException("Code","Tạo hóa đơn thất bại !");
        }

      }
    }

  }

  @Override
  public List<OrderDTO> getOrderByAccountId(Long accountId) {
    var orders=orderRepository.getOrdersByAccountId(accountId);
    ObjectMapper mapper=new ObjectMapper();
    orders.forEach(i->{
      try{
        var products=mapper.readValue(i.getProductsString(),ProductCartDTO[].class);
        i.setProducts(Arrays.asList(products));
      }catch (JsonProcessingException ex)
      {
        i.setProducts(Collections.emptyList());
      }
    });
    return orders;
  }

  @Override
  public void cancelOrder(Long orderId , Long accountId) {
    var orderOptional=orderRepository.findByIdAndAccountId(orderId,accountId);
    if(orderOptional.isPresent())
    {
      orderOptional.get().setOrderStatusId(5L);
      orderRepository.save(orderOptional.get());
    }
  }


}
