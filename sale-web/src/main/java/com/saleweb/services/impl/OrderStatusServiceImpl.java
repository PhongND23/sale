package com.saleweb.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saleweb.domains.Order;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.cart.ProductCartDTO;
import com.saleweb.models.category.DropDownDTO;
import com.saleweb.models.order.OrderDTO;
import com.saleweb.repositories.*;
import com.saleweb.services.CartService;
import com.saleweb.services.OrderService;
import com.saleweb.services.OrderStatusService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderStatusServiceImpl implements OrderStatusService {

  private final OrdersStatusRepository ordersStatusRepository;


  @Override
  public List<DropDownDTO> getDrop() {
    return IterableUtils.toList(ordersStatusRepository.findAll()).stream().map(DropDownDTO::new).collect(Collectors.toList());
  }
}
