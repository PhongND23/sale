package com.saleweb.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saleweb.common.SearchDTO;
import com.saleweb.domains.Product;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.product.ProductCreateDTO;
import com.saleweb.models.product.ProductDTO;
import com.saleweb.models.product.ProductUpdateDTO;
import com.saleweb.repositories.ProductCategoryRepository;
import com.saleweb.repositories.ProductRepository;
import com.saleweb.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

  private final ProductRepository repository;
  private final ProductCategoryRepository productCategoryRepository;
  private final ObjectMapper mapper;

  @Override
  public void create(ProductCreateDTO dto) throws ApplicationException {
    if (StringUtils.isBlank(dto.getName())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tên sản phẩm "));
    }

    if (Objects.isNull(dto.getCategoryId())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Mã danh mục sản phẩm "));
    }

    var categoryOptional = productCategoryRepository.findById(dto.getCategoryId());
    if (categoryOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.NOT_EXIST,
          String.format(
              ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Mã danh mục sản phẩm "));
    }
    if (dto.getImages().isEmpty() || dto.getImages() == null) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(
              ExceptionUtils.messages.get(ExceptionUtils.NULL), "Phải có ít nhất 1 ảnh "));
    }
    if (dto.getPrice() == null) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Giá sản phẩm "));
    }
    if (dto.getPromotionPrice() == null) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Giá khuyến mãi "));
    }
    if (StringUtils.isBlank(dto.getTrademark())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Thương hiệu "));
    }
    if (StringUtils.isBlank(dto.getContent())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Nội dung "));
    }
    if (StringUtils.isBlank(dto.getDescription())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Mô tả "));
    }
    if (StringUtils.isBlank(dto.getClassify())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Phân loại  "));
    }
    if (Objects.isNull(dto.getQuantity())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Số lượng  "));
    }
    if (StringUtils.isBlank(dto.getTags())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tag  "));
    }

    try {
      Product product = new Product(dto);
      var imageString = mapper.writeValueAsString(dto.getImages());
      product.setImages(imageString);
      repository.save(product);
    } catch (JsonProcessingException ex) {
      throw new ApplicationException(
          ExceptionUtils.E_INTERNAL_SERVER, ExceptionUtils.E_INTERNAL_SERVER);
    }
  }

  @Override
  public void update(ProductUpdateDTO dto, Long id) throws ApplicationException {
    Optional<Product> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    if (StringUtils.isBlank(dto.getName())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tên sản phẩm "));
    }

    if (Objects.isNull(dto.getCategoryId())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Mã danh mục sản phẩm "));
    }

    var categoryOptional = productCategoryRepository.findById(dto.getCategoryId());
    if (categoryOptional.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.NOT_EXIST,
          String.format(
              ExceptionUtils.messages.get(ExceptionUtils.NOT_EXIST), "Mã danh mục sản phẩm "));
    }

    if (dto.getPrice() == null) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Giá sản phẩm "));
    }
    if (dto.getPromotionPrice() == null) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Giá khuyến mãi "));
    }
    if (StringUtils.isBlank(dto.getTrademark())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Thương hiệu "));
    }
    if (StringUtils.isBlank(dto.getContent())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Nội dung "));
    }
    if (StringUtils.isBlank(dto.getDescription())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Mô tả "));
    }
    if (StringUtils.isBlank(dto.getClassify())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Phân loại  "));
    }
    if (StringUtils.isBlank(dto.getTags())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tag  "));
    }

    try {

      var imageString = mapper.writeValueAsString(dto.getImages());
      byId.get().setImages(imageString);
      change(byId.get(), dto);
      repository.save(byId.get());
    } catch (JsonProcessingException ex) {

      throw new ApplicationException(
          ExceptionUtils.E_INTERNAL_SERVER, ExceptionUtils.E_INTERNAL_SERVER);
    }
  }

  private void change(Product product, ProductUpdateDTO dto) {
    product.setName(dto.getName());
    product.setCategoryId(dto.getCategoryId());
    product.setTrademark(dto.getTrademark());
    product.setPrice(dto.getPrice());
    product.setPromotionPrice(dto.getPromotionPrice());
    product.setClassify(dto.getClassify());
    product.setQuantity(dto.getQuantity());
    product.setTags(dto.getTags());
    product.setDescription(dto.getDescription());
    product.setContent(dto.getContent());
    product.setStatus(dto.getStatus());
    product.setIsHot(dto.getIsHot());
  }

  @Override
  public void delete(Long id) throws ApplicationException {
    Optional<Product> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }
    repository.delete(byId.get());
  }

  @Override
  public ProductDTO findById(Long id) throws ApplicationException {
    Optional<Product> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_POST_ID_NOT_EXISTS,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_POST_ID_NOT_EXISTS), id));
    }

    try {
      var images = mapper.readValue(byId.get().getImages(), List.class);
      var categoryName =
          productCategoryRepository.findById(byId.get().getCategoryId()).get().getName();
      return new ProductDTO(byId.get(), categoryName, images);
    } catch (JsonProcessingException ex) {
      return new ProductDTO();
    }
  }

  @Override
  public Page<ProductDTO> getAll(SearchDTO dto, Pageable pageable) throws JsonProcessingException {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<Product> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (StringUtils.isNotBlank(dto.getName())) {
            predicates.add(criteriaBuilder.like(root.get("name"), "%" + dto.getName() + "%"));
          }
          if (!Objects.isNull(dto.getCategoryId())) {
            predicates.add(criteriaBuilder.equal(root.get("categoryId"),  dto.getCategoryId()));
          }

          if (!Objects.isNull(dto.getFromPrice())) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("price"),  dto.getFromPrice()));
          }
          if (!Objects.isNull(dto.getToPrice())) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("price"),  dto.getToPrice()));
          }
          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    Page<Product> all = repository.findAll(specification, pageable);
    Map<Long, String> categories = new HashMap<>();
    var categoryList = IterableUtils.toList(productCategoryRepository.findAll());
    categoryList.forEach(
        i -> {
          categories.put(i.getId(), i.getName());
        });
    List<ProductDTO> productDTOList = new ArrayList<>();
    for (Product p : all.getContent()) {
      ProductDTO productDTO = new ProductDTO(p, null, null);
      if (StringUtils.isNotEmpty(p.getImages())) {
        List images = mapper.readValue(p.getImages(), List.class);
        productDTO.setImages(images);
      }
      productDTO.setCategoryName(categories.get(p.getCategoryId()));
      productDTO.setId(p.getId());
      productDTOList.add(productDTO);
    }
    return new PageImpl<>(productDTOList, pageable, all.getTotalElements());
  }

  @Override
  public List<ProductDTO> findAll() {
    var products = IterableUtils.toList(repository.findAll());
    ObjectMapper mapper = new ObjectMapper();
    List<ProductDTO> productDTOList = new ArrayList<>();
    products.forEach(
        i -> {
          ProductDTO productDTO = new ProductDTO(i);
          try {
            var images = mapper.readValue(i.getImages(), String[].class);
            productDTO.setImages(Arrays.asList(images));
            productDTOList.add(productDTO);
          } catch (JsonProcessingException ex) {

          }
        });
    return productDTOList;
  }

  @Override
  public List<ProductDTO> getProductsHot() {
    var products =
        IterableUtils.toList(repository.findAll()).stream()
            .filter(i -> BooleanUtils.isTrue(i.getIsHot())).collect(Collectors.toList());
    ObjectMapper mapper = new ObjectMapper();
    List<ProductDTO> productDTOList = new ArrayList<>();
    products.forEach(
        i -> {
          ProductDTO productDTO = new ProductDTO(i);
          try {
            var images = mapper.readValue(i.getImages(), String[].class);
            productDTO.setImages(Arrays.asList(images));
            productDTOList.add(productDTO);
          } catch (JsonProcessingException ex) {

          }
        });
    return productDTOList;
  }
}
