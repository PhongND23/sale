package com.saleweb.services.impl;

import com.saleweb.common.SearchDTO;
import com.saleweb.domains.PostCategory;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.category.DropDownDTO;
import com.saleweb.models.category.PostCategoryCreateDTO;
import com.saleweb.models.category.PostCategoryDTO;
import com.saleweb.models.category.PostCategoryUpdateDTO;
import com.saleweb.repositories.PostCategoryRepository;
import com.saleweb.services.PostCategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PostCategoryServiceImpl implements PostCategoryService {

  private final PostCategoryRepository repository;

  @Override
  public void create(PostCategoryCreateDTO dto) throws ApplicationException {
    if (StringUtils.isBlank(dto.getName())) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_NAME_NULL,
          ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST));
    }
    repository.save(new PostCategory(dto));
  }

  @Override
  public void update(PostCategoryUpdateDTO dto, Long id) throws ApplicationException {
    Optional<PostCategory> postCategory = repository.findById(id);
    if (postCategory.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    if (StringUtils.isBlank(dto.getName())) {
      throw new ApplicationException(
              ExceptionUtils.NULL,
              String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Tên danh mục "));
    }
   postCategory.get().setName(dto.getName());
   postCategory.get().setDescription(dto.getDescription());
   postCategory.get().setStatus(dto.getStatus());

    repository.save(postCategory.get());
  }

  @Override
  public void delete(Long id) throws ApplicationException {
    if (!repository.existsById(id)) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    repository.deleteById(id);
  }

  @Override
  public PostCategoryDTO findById(Long id) throws ApplicationException {
    Optional<PostCategory> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    return new PostCategoryDTO(byId.get());
  }

  @Override
  public Page<PostCategoryDTO> getAll(SearchDTO dto, Pageable pageable) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<PostCategory> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();

          if (StringUtils.isNotBlank(dto.getName())) {
            predicates.add(criteriaBuilder.like(root.get("name"), "%"+dto.getName()+"%"));
          }
          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    return repository.findAll(specification, pageable).map(PostCategoryDTO::new);
  }

  @Override
  public List<DropDownDTO> getDropDown() {
    return IterableUtils.toList(repository.findAll()).stream()
        .map(DropDownDTO::new)
        .collect(Collectors.toList());
  }
}
