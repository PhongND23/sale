package com.saleweb.services.impl;

import com.saleweb.common.SearchDTO;
import com.saleweb.domains.User;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.exceptions.ExceptionUtils;
import com.saleweb.models.user.UserCreateDTO;
import com.saleweb.models.user.UserDTO;
import com.saleweb.models.user.UserUpdateDTO;
import com.saleweb.repositories.UsersRepository;
import com.saleweb.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

  private final UsersRepository repository;

  @Override
  public void create(UserCreateDTO dto) throws ApplicationException {}

  @Override
  public void update(UserUpdateDTO dto, Long id) throws ApplicationException {
    Optional<User> optionalUsers = repository.findById(id);
    if (optionalUsers.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    if (StringUtils.isBlank(dto.getFullName())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Họ tên "));
    }
    if (StringUtils.isBlank(dto.getAddress())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Địa chỉ "));
    }
    if (StringUtils.isBlank(dto.getEmail())) {
      throw new ApplicationException(
          ExceptionUtils.NULL,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Email "));
    }
      if (StringUtils.isBlank(dto.getPhoneNumber())) {
          throw new ApplicationException(
                  ExceptionUtils.NULL,
                  String.format(ExceptionUtils.messages.get(ExceptionUtils.NULL), "Số điện thoại "));
      }
    optionalUsers.get().setFullName(dto.getFullName());
    optionalUsers.get().setAddress(dto.getAddress());
    optionalUsers.get().setEmail(dto.getEmail());
    optionalUsers.get().setGender(dto.getGender());
    optionalUsers.get().setPhoneNumber(dto.getPhoneNumber());
    optionalUsers.get().setDateOfBirth(dto.getDateOfBirth());
    repository.save(optionalUsers.get());
  }

  @Override
  public void delete(Long id) throws ApplicationException {
    if (!repository.existsById(id)) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    repository.deleteById(id);
  }

  @Override
  public UserDTO findById(Long id) throws ApplicationException {
    Optional<User> byId = repository.findById(id);
    if (byId.isEmpty()) {
      throw new ApplicationException(
          ExceptionUtils.E_CATEGORY_ID_NOT_EXIST,
          String.format(ExceptionUtils.messages.get(ExceptionUtils.E_CATEGORY_ID_NOT_EXIST), id));
    }
    return new UserDTO(byId.get());
  }

  @Override
  public Page<UserDTO> getAll(SearchDTO o, Pageable pageable) {
    Sort sort;
    sort = pageable.getSort();
    pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    Specification<User> specification =
        (root, criteriaQuery, criteriaBuilder) -> {
          List<Predicate> predicates = new ArrayList<>();
          if (StringUtils.isNotBlank(o.getName())) {
            predicates.add(criteriaBuilder.equal(root.get("fullName"), o.getName()));
          }
          if (StringUtils.isNotBlank(o.getAddress())) {
            predicates.add(criteriaBuilder.equal(root.get("address"), o.getAddress()));
          }
          if (StringUtils.isNotBlank(o.getEmail())) {
            predicates.add(criteriaBuilder.equal(root.get("email"), o.getEmail()));
          }
          if (StringUtils.isNotBlank(o.getPhoneNumber())) {
            predicates.add(criteriaBuilder.equal(root.get("phoneNumber"), o.getPhoneNumber()));
          }
          if (StringUtils.isNotBlank(o.getGender())) {
            predicates.add(criteriaBuilder.equal(root.get("gender"), o.getGender()));
          }
          if (null != o.getDateOfBirth()) {
            predicates.add(criteriaBuilder.equal(root.get("dateOfBirth"), o.getDateOfBirth()));
          }
          if (null != o.getStatus()) {
            predicates.add(criteriaBuilder.equal(root.get("status"), o.getStatus()));
          }
          return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
        };
    Page<User> all = repository.findAll(specification, pageable);
    return all.map(UserDTO::new);
  }
}
