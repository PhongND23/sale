package com.saleweb.services;

import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.cart.ProductCartDTO;
import com.saleweb.models.category.DropDownDTO;
import com.saleweb.models.order.OrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrderService {
    void payment(Long account_id,Long paymentId,List<ProductCartDTO> list) throws ApplicationException;

    OrderDTO getOrder(Long id);

    Page<OrderDTO> getAll(Pageable pageable);
    Page<OrderDTO> getAllByStatus(Pageable pageable,Long id);

    void changeStatus(Long id,Long statusId);

    void buyNow(Long accountId,Long productId,Integer quantity,Long paymentId) throws ApplicationException;

    List<OrderDTO> getOrderByAccountId(Long accountId);

    void cancelOrder(Long orderId,Long accountId);


}
