package com.saleweb.services;

import com.saleweb.domains.Account;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.account.*;

public interface AccountService
    extends BaseService<AccountCreateDTO, AccountUpdateDTO, AccountDTO> {
    AccountQueryDTO Login(Login login) throws ApplicationException;
    AccountQueryDTO LoginAdmin(Login login) throws ApplicationException;
    void changePass(Long id,String pass);
}
