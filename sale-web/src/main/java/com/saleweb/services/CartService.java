package com.saleweb.services;

import com.saleweb.domains.Cart;
import com.saleweb.domains.Product;
import com.saleweb.exceptions.ApplicationException;
import com.saleweb.models.cart.CartDTO;
import com.saleweb.models.cart.ProductCartDTO;
import com.saleweb.models.user.UserCreateDTO;
import com.saleweb.models.user.UserDTO;
import com.saleweb.models.user.UserUpdateDTO;

import java.util.List;

public interface CartService {
    void addToCart(Long account_id,Long product_id) throws ApplicationException;
    void addToCart(Long account_id,Long product_id,int quantity) throws ApplicationException;
    void removeToCart(Long account_id,Long product_id) throws ApplicationException;
    void updateCart(Long account_id, List<ProductCartDTO> list) throws ApplicationException;

    CartDTO getCart(Long accountId);

}
