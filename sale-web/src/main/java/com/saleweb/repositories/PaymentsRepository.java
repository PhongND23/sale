package com.saleweb.repositories;

import com.saleweb.domains.Payments;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PaymentsRepository
    extends PagingAndSortingRepository<Payments, Long>, JpaSpecificationExecutor<Payments> {}
