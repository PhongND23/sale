package com.saleweb.repositories;

import com.saleweb.domains.Order;
import com.saleweb.models.order.OrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface OrderRepository
    extends PagingAndSortingRepository<Order, Long>, JpaSpecificationExecutor<Order> {
    Optional<Order> findTopByAccountIdOrderByCreatedDateDesc(Long id);
    @Query(value = "select new com.saleweb.models.order.OrderDTO( od.id,ac.id,us.fullName,us.address,us.email,us.phoneNumber,us.dateOfBirth,\n" +
            "od.ship,os.name,os.id,pa.name,pa.id,od.products,od.createdDate)\n" +
            "from Order od\n" +
            "left join Account ac on od.accountId=ac.id\n" +
            "left join Payments pa on od.paymentId=pa.id\n" +
            "left join OrderStatus os on od.orderStatusId=os.id\n" +
            "left join User us on us.id=ac.userId",
    countQuery = "select count(1)"+"from Order od\n" +
            "left join Account ac on od.accountId=ac.id\n" +
            "left join Payments pa on od.paymentId=pa.id\n" +
            "left join OrderStatus os on od.orderStatusId=os.id\n" +
            "left join User us on us.id=ac.userId")
    Page<OrderDTO> getOrders(Pageable pageable);

  @Query(
      value =
              "select new com.saleweb.models.order.OrderDTO( od.id,ac.id,us.fullName,us.address,us.email,us.phoneNumber,us.dateOfBirth,\n" +
                      "od.ship,os.name,os.id,pa.name,pa.id,od.products,od.createdDate)\n"
              + "from Order od\n"
              + "left join Account ac on od.accountId=ac.id\n"
              + "left join Payments pa on od.paymentId=pa.id\n"
              + "left join OrderStatus os on od.orderStatusId=os.id\n"
              + "left join User us on us.id=ac.userId\n"
              + "where od.id=:id")
  OrderDTO getOderById(Long id);


    @Query(value = "select new com.saleweb.models.order.OrderDTO( od.id,ac.id,us.fullName,us.address,us.email,us.phoneNumber,us.dateOfBirth,\n" +
            "od.ship,os.name,os.id,pa.name,pa.id,od.products,od.createdDate)\n" +
            "from Order od\n" +
            "left join Account ac on od.accountId=ac.id\n" +
            "left join Payments pa on od.paymentId=pa.id\n" +
            "left join OrderStatus os on od.orderStatusId=os.id\n" +
            "left join User us on us.id=ac.userId\n"+
            "where od.orderStatusId=:statusId",
            countQuery = "select count(1)"+"from Order od\n" +
                    "left join Account ac on od.accountId=ac.id\n" +
                    "left join Payments pa on od.paymentId=pa.id\n" +
                    "left join OrderStatus os on od.orderStatusId=os.id\n" +
                    "left join User us on us.id=ac.userId\n"+
                    "where od.orderStatusId=:statusId")
    Page<OrderDTO> getOrdersByStatus(Pageable pageable,Long statusId);


    @Query(value = "select new com.saleweb.models.order.OrderDTO( od.id,ac.id,us.fullName,us.address,us.email,us.phoneNumber,us.dateOfBirth,\n" +
            "od.ship,os.name,os.id,pa.name,pa.id,od.products,od.createdDate)\n" +
            "from Order od\n" +
            "left join Account ac on od.accountId=ac.id\n" +
            "left join Payments pa on od.paymentId=pa.id\n" +
            "left join OrderStatus os on od.orderStatusId=os.id\n" +
            "left join User us on us.id=ac.userId\n"+
            "where od.accountId=:accountId\n"+
    " order by  od.createdDate DESC ")
    List<OrderDTO> getOrdersByAccountId(Long accountId);

    Optional<Order> findByIdAndAccountId(Long accountId,Long orderId);
}
