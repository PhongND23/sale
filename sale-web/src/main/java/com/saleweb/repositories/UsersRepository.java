package com.saleweb.repositories;

import com.saleweb.domains.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UsersRepository
    extends PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User> {}
