package com.saleweb.repositories;

import com.saleweb.domains.Role;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleRepository
    extends PagingAndSortingRepository<Role, Long>, JpaSpecificationExecutor<Role> {}
