-- SEQUENCE: public.account_sequence_id

-- DROP SEQUENCE IF EXISTS public.account_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.account_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY account.id;

ALTER SEQUENCE public.account_sequence_id
    OWNER TO postgres;
	
	
	-- SEQUENCE: public.cart_detail_sequence_id

-- DROP SEQUENCE IF EXISTS public.cart_detail_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.cart_detail_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1;

ALTER SEQUENCE public.cart_detail_sequence_id
    OWNER TO postgres;
	
	-- SEQUENCE: public.cart_sequence_id

-- DROP SEQUENCE IF EXISTS public.cart_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.cart_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY cart.id;

ALTER SEQUENCE public.cart_sequence_id
    OWNER TO postgres;
	
	-- SEQUENCE: public.order_detail_sequence_id

-- DROP SEQUENCE IF EXISTS public.order_detail_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.order_detail_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY order_detail.id;

ALTER SEQUENCE public.order_detail_sequence_id
    OWNER TO postgres;
	
	-- SEQUENCE: public.order_sequence_id

-- DROP SEQUENCE IF EXISTS public.order_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.order_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY orders.id;

ALTER SEQUENCE public.order_sequence_id
    OWNER TO postgres;
	
	
	-- SEQUENCE: public.order_status_sequence_id

-- DROP SEQUENCE IF EXISTS public.order_status_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.order_status_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY order_status.id;

ALTER SEQUENCE public.order_status_sequence_id
    OWNER TO postgres;
	
	
	-- SEQUENCE: public.payments_sequence_id

-- DROP SEQUENCE IF EXISTS public.payments_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.payments_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY payments.id;

ALTER SEQUENCE public.payments_sequence_id
    OWNER TO postgres;
	
	-- SEQUENCE: public.post_category_sequence_id

-- DROP SEQUENCE IF EXISTS public.post_category_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.post_category_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY post_category.id;

ALTER SEQUENCE public.post_category_sequence_id
    OWNER TO postgres;
	
	-- SEQUENCE: public.post_sequence_id

-- DROP SEQUENCE IF EXISTS public.post_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.post_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY post.id;

ALTER SEQUENCE public.post_sequence_id
    OWNER TO postgres;
	
	-- SEQUENCE: public.product_category_sequence_id

-- DROP SEQUENCE IF EXISTS public.product_category_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.product_category_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY post_category.id;

ALTER SEQUENCE public.product_category_sequence_id
    OWNER TO postgres;
	
	
	
	-- SEQUENCE: public.product_sequence_id

-- DROP SEQUENCE IF EXISTS public.product_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.product_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY product.id;

ALTER SEQUENCE public.product_sequence_id
    OWNER TO postgres;
	
	-- SEQUENCE: public.role_sequence_id

-- DROP SEQUENCE IF EXISTS public.role_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.role_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY role.id;

ALTER SEQUENCE public.role_sequence_id
    OWNER TO postgres;
	
	
	-- SEQUENCE: public.tag_sequence_id

-- DROP SEQUENCE IF EXISTS public.tag_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.tag_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY tag.id;

ALTER SEQUENCE public.tag_sequence_id
    OWNER TO postgres;
	
	-- SEQUENCE: public.users_sequence_id

-- DROP SEQUENCE IF EXISTS public.users_sequence_id;

CREATE SEQUENCE IF NOT EXISTS public.users_sequence_id
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 1000000000
    CACHE 1
    OWNED BY users.id;

ALTER SEQUENCE public.users_sequence_id
    OWNER TO postgres;
	
	
	